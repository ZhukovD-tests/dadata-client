// import css from '../../styles/ResumeForm.module.scss'
import { useEffect, useRef } from 'react';

function InputForm ({ value, onClick }) {

    const
        input = useRef(null);

    useEffect(() => {
        input.current.value = value
    }, [])

    const handleSubmit = (e) => {
        e.preventDefault();
        onClick(input.current.value);
        // input.current.value = null;
    } 

    return (
        <form>
            <input ref={input} />
            <input type="submit" value="Запросить" onClick={handleSubmit} />
        </form>
    )
}
export default InputForm;