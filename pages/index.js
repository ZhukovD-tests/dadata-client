import * as db from './../db/connect';
import Head from 'next/head'
import * as sendRequest from './../functions/sendRequest'
import InputForm from '../components/InputForm';
import OutputArea from '../components/OutputArea';
import styles from '../styles/Home.module.css'
import { useState } from 'react';

export async function getServerSideProps(context) {

  let query = "мск сухонска 11/-89";
  
  const getRandom = (amount) => {
    return Math.ceil(Math.random() * amount)
  }

  await new Promise((resolve, reject) => {
    db.all(`SELECT request FROM requests`, (err, rows) => {
      if (err) {
        console.error(err.message);
        resolve(false);
      }
      if (rows) {
        let rowIndex = getRandom(rows.length - 1);
        if(rows[rowIndex] && rows[rowIndex].request) query = rows[rowIndex].request; 
        resolve(true);
      }
    });
  })

  let 
      data = await sendRequest(query);

  data = JSON.stringify(data, null, "\t");
  
  return { props: { data, query } }
}

export default function Home( {data, query} ) {

  const 
        [responseData, setResponseData] = useState(data);

  const sendRequest = (query) => {
    
    let 
      options = {
        method: "POST",
        body: query
      }

    fetch("api/info", options)
      .then(resRaw => resRaw.json())
      .then(res => {
        setResponseData(JSON.stringify(res, null, "\t"))
      })

  } 

  return (
    <div className={styles.container}>
      <Head>
        <title>DaData requester</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <InputForm value={query} onClick={sendRequest} />
        <OutputArea data={responseData} />
      </main>
    </div>
  )
}
