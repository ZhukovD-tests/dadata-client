// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
let db = require("./../../db/connect");
let sendRequest = require('./../../functions/sendRequest')

export default async (req, res) => {

  let query = req.body;

  if(query) {
    db.serialize(() => {
      db.run( 'INSERT INTO requests (request) VALUES (?)', [query] )
    });
  }

  let data = await sendRequest(query);

  res.statusCode = 200
  res.json(data)
}
