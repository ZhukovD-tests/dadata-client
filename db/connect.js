const sqlite3 = require('sqlite3').verbose();

db = new sqlite3.Database('./requests.db',
    sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, 
    (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Connected to the SQlite database.');
    });
   
const createTableQuery = `
    CREATE TABLE IF NOT EXISTS requests (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      request TEXT)`

db.run(createTableQuery)

module.exports = db;