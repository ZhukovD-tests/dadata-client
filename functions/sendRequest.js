require('dotenv').config();

async function sendRequest(query = "мск сухонска 11/-89") {

    let
        url = process.env.API_URL,
        token = process.env.API_TOKEN,
        secret = process.env.API_SECRET;

    let
        options = {
            method: "POST",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Token " + token,
                "X-Secret": secret
            },
            body: JSON.stringify([query])
        }

    let
        res = await fetch(url, options),
        data = await res.json().catch((err)=>{return {error: err}});

    return data
}
module.exports = sendRequest;